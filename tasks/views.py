from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.owner = request.user
            task.save()
            return redirect("create_project")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)


@login_required
def show_task(request):
    pass
